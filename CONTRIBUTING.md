# Contribuyendo

Cuando se contribuya en este repositorio, por favor primero debes informar del cambio que desees aportar vía Incidencias (Issues), Whatsapp, Telegram ó email.

Le ruego guarde el código de conducta durante sus interacciones con el proyecto, tanto a nivel de comentarios como de código.

## Cómo comenzar a trabajar (Clone)

1. Haz un ***fork*** del proyecto
2. Dale a **Clone** en el ***fork*** que has creado y copia el link de **Clone with HTTPS**
3. Desde la linea de comandos: **git clone ...**
4. En **Eclipse** (con Java JRE 13) dale a la opción de **Import...**
5. Despliega **General** y selecciona **Existing Projects into Workspace**

> ¡Importante: si se trabaja con otra versión distinta a la de Java 13, no añadir ni hacer commit del archivo **.classpath** !

----

## Código de Conducta

### Compromiso

En aras de fomentar un ambiente abierto y acogedor, nosotros, como colaboradores y mantenedores, nos comprometemos a hacer que la participación en nuestro proyecto y nuestra comunidad sea una experiencia libre de acoso para todos, independientemente de su edad, tamaño corporal, discapacidad, origen étnico, identidad y expresión de género, nivel de experiencia, nacionalidad, apariencia personal, raza, religión o identidad y orientación sexual.

### Normas

Ejemplos de comportamiento inaceptable:

* Uso de imágenes o lenguaje sexualizados
* Comentarios despectivos o insultantes
* Ataques personales o políticos
* Acoso público o privado
* Publicación de información ajena, privada y/o confidencial