![](https://gitlab.com/Baeolian/tamagetsii/raw/master/src/images/Logo.png)

### Cómo Contribuir
Ver [CONTRIBUTING.md](CONTRIBUTING.md)

----

## Tareas a realizar

> Se rechazará todo **MR** que varíe la estructura de archivos del repositorio

### Fase 0
- [x] Iniciar el repositorio
- [x] Comprender el código (Autores)
    - [x] Ayuso Martínez, Álvaro
    - [x] Cote Llamas, José
    - [x] Iglesias Trujillo, Carmen ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/5)
    - [x] Pérez Peña, Antonio

#### Fase 1
- [x] Organizar proyecto por clases en distintos archivos
(Todo está en *Main.java*)
    - [x] [AnimationFrame.java](https://gitlab.com/Baeolian/tamagetsii/issues/2) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/6)
    - [x] [Logic.java](https://gitlab.com/Baeolian/tamagetsii/issues/1) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/8)
    - [x] [Animation.java](https://gitlab.com/Baeolian/tamagetsii/issues/3) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/1)
    - [x] [Animations.java](https://gitlab.com/Baeolian/tamagetsii/issues/4) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/2)
    - [x] [Components.java](https://gitlab.com/Baeolian/tamagetsii/issues/5) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/4)

#### Fase 2
- [x] [Búsqueda de bugs](https://gitlab.com/Baeolian/tamagetsii/issues/6) (descubrir y reportar)
- [x] Bug fixing
    - [x] [UI: Desplazado Texto de STATS](https://gitlab.com/Baeolian/tamagetsii/issues/7) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/11)
    - [x] [UI: Desplazado Círculo de Botón Izquierdo](https://gitlab.com/Baeolian/tamagetsii/issues/8) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/12)
    - [x] [Logic: La edad siempre es 0](https://gitlab.com/Baeolian/tamagetsii/issues/9) ➦ [MR](https://gitlab.com/Baeolian/tamagetsii/merge_requests/13)

----

### Licencia
Ver [LICENSE.md](LICENSE.md)
